package com.reactivestax.threads.type2;

import lombok.SneakyThrows;

import java.io.IOException;

public class Launcher implements Runnable{
    @SneakyThrows
    public static void main(String[] args) {
        Thread fSplitter = new Thread(new Launcher());
        fSplitter.start();
        fSplitter.join();
        Thread t1 = new Thread(new Launcher());
        t1.start();
        Thread t2 = new Thread(new Launcher());
        t2.start();
        Thread t3 = new Thread(new Launcher());
        t3.start();
        Thread t4 = new Thread(new Launcher());
        t4.start();
    }

    @Override
    public void run() {
        try {
            FileSplitter.doSplit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
