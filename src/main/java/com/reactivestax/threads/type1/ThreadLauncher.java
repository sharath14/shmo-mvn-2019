package com.reactivestax.threads.type1;

import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.Map;

public class ThreadLauncher implements Runnable {
        static Map<String, Boolean> mapOfFiles = new HashMap<>();
        static String file1 = "/Users/sharathchandra/Downloads/1.csv";
        static String file2 = "/Users/sharathchandra/Downloads/2.csv";
        static String file3 = "/Users/sharathchandra/Downloads/3.csv";
        static String file4 = "/Users/sharathchandra/Downloads/4.csv";
        static String filepath = null;
    @SneakyThrows
    @Override
    public void run() {
        FileParser fileParser = new FileParser();
        fileParser.doFileParsing(decideFilePath(filepath));
    }
    public static synchronized String decideFilePath(String filepath){
        for (Map.Entry<String, Boolean> entry : mapOfFiles.entrySet()) {
            if (entry.getValue() == false ) {
                mapOfFiles.replace(entry.getKey(), true);
                System.out.println(Thread.currentThread().getName());
                System.out.println("value"+entry.getKey());
               return ThreadLauncher.filepath = entry.getKey();
            }
        }
        return filepath;
    }
}
