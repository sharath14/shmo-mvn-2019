package com.reactivestax.threads.type3;

import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Launcher3 {
    public static void main(String[] args) throws SQLException {
            String path="/Users/sharathchandra/Downloads/banktransactions.csv";
            BlockingQueue<Transaction> blockingQueue =
                    new LinkedBlockingQueue<Transaction>();
            Thread producer = new Thread(new ReaderLogicWIthQueue(path,blockingQueue));
            Thread consumer = new Thread(new WritterLogicWithQueue (blockingQueue));
            producer.start();
            consumer.start();
    }
}
