package com.reactivestax.file;

import lombok.Data;
import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

@Data
public class FileParser {
    public void doFileParsing() throws SQLException {
//        Assert.isTrue(getTransactionDAO() != null, "transactionsDAO cannot be null, check your bean Wiring in .xml file");

        String[] columns;
        String line;
        File file = new File("/Users/sharathchandra/Downloads/banktransactions.csv");
        BufferedReader brdr;
        try {
            brdr = new BufferedReader(new FileReader(file));
            brdr.readLine();
            while ((line = brdr.readLine()) != null) {
                columns = line.split(",");
                if (columns.length > 8) {
                    continue;
                }
                Transaction transaction = new Transaction();
                transaction.setAccount_No(columns[0]);
                transaction.setTransactionDate(DataHelper.parseDate(columns[1]));
                transaction.setTransactionDetails(columns[2]);
                transaction.setChqNo(columns[3]);
                transaction.setValueDate(DataHelper.parseDate(columns[4]));
                transaction.setWithdrawalAmt(DataHelper.StringToDouble(columns[5]));
                transaction.setDepositAmt(DataHelper.StringToDouble(columns[6]));
                transaction.setBalanceAmt(DataHelper.StringToDouble(columns[7]));
                TransactionDAO transactionDAO = new TransactionDAOImpl();
                transactionDAO.readIntoDatabase(transaction);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
