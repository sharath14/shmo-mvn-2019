package com.reactivestax.threads.type1;

import lombok.Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

@Data
public class FileParser {
    Transaction transaction = new Transaction();
    TransactionDAOImpl transactionDAO;

    {
        try {
            transactionDAO = new TransactionDAOImpl();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void doFileParsing(String filepath) {
        String[] columns;
        String line;
        File file = new File(filepath);
        BufferedReader brdr;
        try {
            brdr = new BufferedReader(new FileReader(file));
            brdr.readLine();
            System.out.println("brdr.readLine() = " + brdr.readLine());
            while ((line = brdr.readLine()) != null) {
                columns = line.split(",");
                System.out.println("columns"+ columns[0]+ columns[1]+columns[2]+columns[3]+columns[4]+columns[5]);
                if (columns.length > 8) {
                    continue;
                }
                transaction.setAccount_No(columns[0]);
                transaction.setTransactionDate(DataHelper.parseDate(columns[1]));
                transaction.setTransactionDetails(columns[2]);
                transaction.setChqNo(columns[3]);
                transaction.setValueDate(DataHelper.parseDate(columns[4]));
                transaction.setWithdrawalAmt(DataHelper.StringToDouble(columns[5]));
                transaction.setDepositAmt(DataHelper.StringToDouble(columns[6]));
                transaction.setBalanceAmt(DataHelper.StringToDouble(columns[7]));
                transactionDAO.readIntoDatabase(transaction);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
