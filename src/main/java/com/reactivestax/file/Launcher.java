package com.reactivestax.file;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Launcher {
    public static void main(String[] args) throws Exception {
//        ClassPathXmlApplicationContext xmlApplicationContext = new ClassPathXmlApplicationContext("TransactionFile.xml");
//        FileParser fileParser = xmlApplicationContext.getBean("fileParser", FileParser.class);
//        fileParser.doFileParsing();
        FileParser fileParser = new FileParser();
        fileParser.doFileParsing();
    }
}
