package com.reactivestax.threads.type1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionDAOImpl implements TransactionDAO<Transaction> {

    String insertSqlQuery = "Insert into Transactions (Account_No,TRANSACTION_DATE,TRANSACTION_DETAILS,CHQ_NO,VALUE_DATE,WITHDRAWAL_AMT,DEPOSIT_AMT,BALANCE_AMT)values(?,?,?,?,?,?,?,?)";
    Connection connection = ConnectionFactory.getConnection();
    PreparedStatement preparedStatement;
    public TransactionDAOImpl() throws SQLException {
    }

    @Override
    public synchronized void readIntoDatabase(Transaction transaction) {
        try {
            preparedStatement = connection.prepareStatement(insertSqlQuery);
            System.out.println("transaction = " + transaction);
            preparedStatement.setString(1, transaction.getAccount_No());
            preparedStatement.setDate(2, new java.sql.Date(transaction.getTransactionDate().getTime()));
            preparedStatement.setString(3, transaction.getTransactionDetails());
            preparedStatement.setString(4, transaction.getChqNo());
            preparedStatement.setDate(5, new java.sql.Date(transaction.getValueDate().getTime()));
            preparedStatement.setDouble(6, transaction.getWithdrawalAmt());
            preparedStatement.setDouble(7, transaction.getDepositAmt());
            preparedStatement.setDouble(8, transaction.getBalanceAmt());
            preparedStatement.addBatch();
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                preparedStatement.close();
                //connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}

