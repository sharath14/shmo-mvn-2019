package com.reactivestax.threads.type2;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileSplitter{
    public static void doSplit() throws IOException {
        List<String> fileList = new ArrayList<>();
        Integer noOfLines = 0;
        File file = new File("/Users/sharathchandra/Downloads/banktransactions.csv");
        BufferedReader br1 = new BufferedReader(new FileReader(file));
        BufferedReader br2 = new BufferedReader(new FileReader(file));
        BufferedWriter bw = null;
        Integer firstLine;
        Integer lastLine;
        Integer part = 1;
        while (br1.readLine() != null) {
            noOfLines++;
        }
        System.out.println(noOfLines);
        Integer linePerPart = (Integer) noOfLines / 4;
        firstLine = 1;
        lastLine = linePerPart + 1;
        while (part <= 4) {
            String filePath = "/Users/sharathchandra/Downloads/" + part + ".csv";
            File file1copy = new File(filePath);
            bw = new BufferedWriter(new FileWriter(file1copy));
            for (int i = firstLine; i <= lastLine; i++) {
                String line = br2.readLine();
                bw.write(line + "\n");
                bw.flush();
            }
            fileList.add(filePath);
            bw.flush();
            firstLine = (Integer) lastLine + 1;
            lastLine = (Integer) lastLine + linePerPart;
            part++;

        }
        bw.close();
        br1.close();
        br2.close();


    }
}