package com.reactivestax.threads.type1;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Transaction {
    String account_No;
    Date transactionDate;
    String transactionDetails;
    String chqNo;
    Date valueDate;
    Double withdrawalAmt;
    Double depositAmt;
    Double balanceAmt;
    @Override
    public String toString() {
        return "Transactions{" +
                "Account_No='" + account_No + '\'' +
                ", TRANSACTION_DATE=" + transactionDate +
                ", TRANSACTION_DETAILS='" + transactionDetails + '\'' +
                ", CHQ_NO='" + chqNo + '\'' +
                ", VALUE_DATE=" + valueDate +
                ", WITHDRAWAL_AMT=" + withdrawalAmt +
                ", DEPOSIT_AMT=" + depositAmt +
                ", BALANCE_AMT=" + balanceAmt +
                '}';
    }

}
