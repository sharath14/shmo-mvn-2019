package com.reactivestax.threads.type3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.BlockingQueue;

public class ReaderLogicWIthQueue implements Runnable {
    private String path;
    private final BlockingQueue<Transaction> blockingQueue;
    public ReaderLogicWIthQueue(String path, BlockingQueue<Transaction> blockingQueue) {
        this.path = path;
        this.blockingQueue = blockingQueue;
    }
    @Override
    public void run() {
        try {
            BufferedReader bs = new BufferedReader(new FileReader(path));
            String lineText = null;
            int count = 0;
            String line = bs.readLine();
            while ((lineText = bs.readLine()) != null) {
                String[] columns = lineText.split(",");
                if (columns.length > 8) {
                    continue;
                }
                Transaction transactions = new Transaction();
                transactions.setAccount_No(columns[0]);
                transactions.setTransactionDate(DataHelper.parseDate(columns[1]));
                transactions.setTransactionDetails(columns[2]);
                transactions.setChqNo(columns[3]);
                transactions.setValueDate(DataHelper.parseDate(columns[4]));
                transactions.setWithdrawalAmt(DataHelper.StringToDouble(columns[5]));
                transactions.setDepositAmt(DataHelper.StringToDouble(columns[6]));
                transactions.setBalanceAmt(DataHelper.StringToDouble(columns[7]));
                blockingQueue.put(transactions);
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}

