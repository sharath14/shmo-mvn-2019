package com.reactivestax.threads.type1;

import java.sql.SQLException;

public interface TransactionDAO<T> {
    void readIntoDatabase(Transaction transaction) throws SQLException;
}
