package com.reactivestax.threads.type1;

import static com.reactivestax.threads.type1.ThreadLauncher.*;

public class ThreadLauncher1 {
    public static void main(String[]args){
        mapOfFiles.put(file1, false);
        mapOfFiles.put(file2, false);
        mapOfFiles.put(file3, false);
        mapOfFiles.put(file4, false);
        Thread t1 = new Thread(new ThreadLauncher());
        t1.start();
        Thread t2 = new Thread(new ThreadLauncher());
        t2.start();
        Thread t3 = new Thread(new ThreadLauncher());
        t3.start();
        Thread t4 = new Thread(new ThreadLauncher());
        t4.start();
    }
}
