package com.reactivestax.file;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataHelper {
    static SimpleDateFormat dateFormat =new SimpleDateFormat("dd-MMM-yy");;

    public static Date parseDate(String inputDate) throws ParseException {
        if(inputDate.isEmpty() || inputDate==null){
            return null;
        }
        try {
            Date date = dateFormat.parse(inputDate);
            return date;
        } catch (ParseException pe) {
            throw new ParseException("cannot convert into string", pe.getErrorOffset());
        }
    }
    public static Double StringToDouble(String inputString) {
        if (inputString.isEmpty()) {
            return new Double(0.0);
        }
        return Double.parseDouble(inputString);

    }
}
